// D00215637 Paudric Smith
package gd2.gd2_ca2_paudricsmith_petapp;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main
{

    private static Pets pets = new Pets();
    private static Pets petsFromTxt = new Pets();

    private static String ANSI_RESET = "\u001B[0m";
    private static String ANSI_YELLOW = "\u001B[33m";
    private static String ANSI_CYAN = "\u001B[36m";

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException, ParseException
    {
        Pet.readPetOS();
        //readPetDetailsFromTextFile();

//        if (Pet.readPetDetailsFromTextFile(petDetailsTxtFile) != null)
//        {
//            Map<Pet, Owner> tempPets = new LinkedHashMap<>();
//            tempPets = Pet.readPetDetailsFromTextFile(petDetailsTxtFile);
//            petsFromTxt.getPetsFromTxt().putAll(tempPets);
//
//        }
        System.out.println("Deserialized HashMap pets..");
        // Display content using Iterator
        Set set = pets.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext())
        {
            Map.Entry entry = (Map.Entry) iterator.next();
            System.out.print(entry.getKey() + " & Value: ");
            System.out.println(entry.getValue());

        }

        System.out.println("From text file HashMap pets..");
        // Display content using Iterator
        Set set2 = pets.getPets().entrySet();
        Iterator iterator2 = set2.iterator();
        while (iterator.hasNext())
        {
            Map.Entry entry = (Map.Entry) iterator2.next();
            System.out.print("key: " + entry.getKey() + " & Value: ");
            System.out.println(entry.getValue());

        }

        printHeading();
        printOptions();
        printMainSwitch();

    }

    private static void readPetDetailsFromTextFile() throws ParseException
    {

        try (Scanner scanner = new Scanner(new FileReader("petDetailsFile.txt")))
        {

            scanner.useDelimiter(",");
            while (scanner.hasNextLine())
            {
                String petId = scanner.next();
                scanner.skip(scanner.delimiter());
                String petOwnerId = scanner.next();
                scanner.skip(scanner.delimiter());
                String petType = scanner.next();
                scanner.skip(scanner.delimiter());
                switch (petType)
                {
                    case "fish":

                        String waterType = scanner.next();
                        scanner.skip(scanner.delimiter());
                        String petName = scanner.next();
                        scanner.skip(scanner.delimiter());
                        String breed = scanner.next();
                        scanner.skip(scanner.delimiter());
                        String colour = scanner.next();
                        scanner.skip(scanner.delimiter());
                        String gender = scanner.next();
                        scanner.skip(scanner.delimiter());
                        String date = scanner.next();
                        scanner.skip(scanner.delimiter());
                        int age = scanner.nextInt();
                        scanner.skip(scanner.delimiter());
                        String ownId = scanner.next();
                        scanner.skip(scanner.delimiter());
                        String name = scanner.next();
                        scanner.skip(scanner.delimiter());
                        String email = scanner.next();
                        scanner.skip(scanner.delimiter());
                        String telephone = scanner.next();
                        scanner.skip(scanner.delimiter());
                        String address = scanner.next();
                        scanner.skip(scanner.delimiter());

                        Owner owner = new Owner(name, email, telephone, address);
                        owner.setOwnerId(ownId);
                        Fish tempFish = new Fish(petType, petName, breed, colour, gender, age, waterType);
                        tempFish.setPetId(petId);
                        tempFish.setPetId(petOwnerId);
                        tempFish.setDateRegistered(date);

                        Map<Fish, Owner> fishMap = new LinkedHashMap<>();
                        fishMap.put(tempFish, owner);
                        Map<Pet, Owner> petFishMap = new LinkedHashMap<>();
                        petFishMap.putAll(fishMap);

                        petsFromTxt.putAll(petFishMap);
                        break;

                    case "bird":

                        Double wingSpan = scanner.nextDouble();
                        scanner.skip(scanner.delimiter());
                        boolean canFly = scanner.nextBoolean();
                        scanner.skip(scanner.delimiter());
                        petName = scanner.next();
                        scanner.skip(scanner.delimiter());
                        breed = scanner.next();
                        scanner.skip(scanner.delimiter());
                        colour = scanner.next();
                        scanner.skip(scanner.delimiter());
                        gender = scanner.next();
                        scanner.skip(scanner.delimiter());
                        date = scanner.next();
                        scanner.skip(scanner.delimiter());
                        age = scanner.nextInt();
                        scanner.skip(scanner.delimiter());
                        ownId = scanner.next();
                        scanner.skip(scanner.delimiter());
                        name = scanner.next();
                        scanner.skip(scanner.delimiter());
                        email = scanner.next();
                        scanner.skip(scanner.delimiter());
                        telephone = scanner.next();
                        scanner.skip(scanner.delimiter());
                        address = scanner.next();
                        scanner.skip(scanner.delimiter());

                        owner = new Owner(name, email, telephone, address);
                        owner.setOwnerId(ownId);
                        Bird tempBird = new Bird(petType, petName, breed, colour, gender, age, wingSpan, canFly);
                        tempBird.setPetId(petId);
                        tempBird.setPetId(petOwnerId);
                        tempBird.setDateRegistered(date);

                        Map<Bird, Owner> birdMap = new LinkedHashMap<>();
                        birdMap.put(tempBird, owner);
                        Map<Pet, Owner> petBirdMap = new LinkedHashMap<>();
                        petBirdMap.putAll(birdMap);

                        petsFromTxt.putAll(petBirdMap);
                        break;
                    default:

                        boolean isNeutered = scanner.nextBoolean();
                        scanner.skip(scanner.delimiter());
                        petName = scanner.next();
                        scanner.skip(scanner.delimiter());
                        breed = scanner.next();
                        scanner.skip(scanner.delimiter());
                        colour = scanner.next();
                        scanner.skip(scanner.delimiter());
                        gender = scanner.next();
                        scanner.skip(scanner.delimiter());
                        date = scanner.next();
                        scanner.skip(scanner.delimiter());
                        age = scanner.nextInt();
                        scanner.skip(scanner.delimiter());
                        ownId = scanner.next();
                        scanner.skip(scanner.delimiter());
                        name = scanner.next();
                        scanner.skip(scanner.delimiter());
                        email = scanner.next();
                        scanner.skip(scanner.delimiter());
                        telephone = scanner.next();
                        scanner.skip(scanner.delimiter());
                        address = scanner.next();
                        scanner.skip(scanner.delimiter());

                        owner = new Owner(name, email, telephone, address);
                        owner.setOwnerId(ownId);
                        Mammal tempMammal = new Mammal(petType, petName, breed, colour, gender, age, isNeutered);
                        tempMammal.setPetId(petId);
                        tempMammal.setPetId(petOwnerId);
                        tempMammal.setDateRegistered(date);

                        Map<Mammal, Owner> mammalMap = new LinkedHashMap<>();
                        mammalMap.put(tempMammal, owner);
                        Map<Pet, Owner> petMammalMap = new LinkedHashMap<>();
                        petMammalMap.putAll(mammalMap);

                        petsFromTxt.putAll(petMammalMap);
                        break;
                }
            }

        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            scanner.close();
        }

    }

    private static void printHeading()
    {
        System.out.println("************************************************");
        System.out.println("************************************************");
        System.out.println("***" + ANSI_CYAN + "          Dundalk Pet Registry" + ANSI_RESET + "            ***");
        System.out.println("************************************************");
        System.out.println("************************************************");
    }

    private static void printOptions()
    {
        System.out.println("\n" + ANSI_CYAN + "Please enter one of the following:");
        System.out.println("\t0" + ANSI_RESET + " - To print options:");
        System.out.println("\t" + ANSI_CYAN + "1" + ANSI_RESET + " - Add pet:");
        System.out.println("\t" + ANSI_CYAN + "2" + ANSI_RESET + " - Edit pet:");
        System.out.println("\t" + ANSI_CYAN + "3" + ANSI_RESET + " - Delete pet:");
        System.out.println("\t" + ANSI_CYAN + "4" + ANSI_RESET + " - Print pet details:");
        System.out.println("\t" + ANSI_CYAN + "5" + ANSI_RESET + " - Add pet owner:");
        System.out.println("\t" + ANSI_CYAN + "6" + ANSI_RESET + " - To Quit:");
        System.out.println(ANSI_YELLOW + "Entering Control D will break the App!!!\n" + ANSI_RESET);
    }

    private static void printMainSwitch()
    {
        boolean quit = false;
        boolean isInt = false;
        int choice = 0;

        do
        {

            choice = retrieveInt("Enter choice: ");
            switch (choice)
            {
                case 0:
                    printOptions();
                    break;
                case 1:
                    addPet();
                    break;
                case 2:
                    editPet();
                    break;
                case 3:
                    deletePet();
                    break;
                case 4:
                    printPetDetails();
                    break;
                case 5:
                    addOwner();
                    break;
                case 6:
                    System.out.println("\nQuiting...");
                    quit = true;
                    break;
                default:
                    System.out.println("\nOnly enter an integer from 0 - 8");
                    System.out.println("Enter an option:");
                    break;
            }
        } while (choice < 1 || choice > 6 || isInt == false || quit == false);
    }

    private static String retrieveString(String question)
    {
        boolean tryAgain;
        String onlyLettersAndSpaceRegX = "[a-z\\s]{1,20}";
        String emailRegex = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$";
        String telephoneRegex = "[0-9]{10,20}";
        String userInput;
        System.out.print(question);

        if ("\nEnter your email: ".equals(question))
        {
            do
            {
                tryAgain = false;

                userInput = scanner.nextLine().toLowerCase();
                if (!userInput.matches(emailRegex))
                {
                    System.out.println("Email must be in format \"something@something.com\"!");
                    System.out.print(question);
                    tryAgain = true;
                }

            } while (tryAgain == true);
            return userInput;
        } else if ("\nEnter your telephone: ".equals(question))
        {
            do
            {
                tryAgain = false;

                userInput = scanner.nextLine().toLowerCase();
                if (!userInput.matches(telephoneRegex))
                {
                    System.out.println("Telephone number must be more than 9 numbers only!");
                    System.out.print(question);
                    tryAgain = true;
                }

            } while (tryAgain == true);
            return userInput;
        } else
        {
            do
            {
                tryAgain = false;
                userInput = scanner.nextLine().toLowerCase();

                if (!userInput.matches(onlyLettersAndSpaceRegX))
                {
                    System.out.println("Only letters with no numbers allowed!");
                    System.out.print(question);
                    tryAgain = true;
                }

            } while (tryAgain == true);

            return userInput;
        }

    }

    private static boolean retrieveBoolean(String question)
    {
        boolean tryAgain = false;

        do
        {
            System.out.print(question);
            String userInput = scanner.nextLine();

            if (userInput.equals("yes") || userInput.equals("y"))
            {
                return true;
            } else if (userInput.equals("no") || userInput.equals("n"))
            {
                return false;
            } else
            {
                System.out.print("Enter yes/y or no/n:");
                tryAgain = true;
            }
        } while (tryAgain == true);

        return false;
    }

    private static int retrieveInt(String question)
    {

        System.out.print(question);

        boolean isInt = false;

        do
        {

            // Prevent user from entering anything other than an integer for the choice
            if (scanner.hasNextInt())
            {
                isInt = true;
                int userInput = scanner.nextInt();
                scanner.nextLine();
                return userInput;

            } else
            {
                System.out.println("Enter a number from 0 - 9");
                isInt = false;
                scanner.nextLine();
            }

        } while (isInt == false);

        return 0;

    }

    private static double retrieveDouble(String question)
    {

        System.out.println(question);

        boolean isDouble = false;

        do
        {

            // Prevent user from entering anything other than a double for the choice
            if (scanner.hasNextDouble())
            {
                isDouble = true;
                double userInput = scanner.nextDouble();
                scanner.nextLine();
                return userInput;

            } else
            {
                System.out.println("Enter a number from 0 - 9");
                isDouble = false;
                scanner.nextLine();
            }

        } while (isDouble == false);

        return 0;

    }

    private static void addPet()
    {

        String waterType = null;
        double wingSpan = 0;
        boolean canFly = false;
        boolean neutered = false;
        String name;
        String breed;
        String colour;
        String gender;
        String answer;
        int age;

        // only letters whitespace and no numbers
        String onlyLettersAndSpaceRegX = "[a-z\\s]{1,20}";
        // musht contain an @ once
        //String emailRegX = "[@]{1}";
        boolean tryAgain = false;
        boolean exit = false;

        do
        {
            String type = retrieveString("\nEnter pet Type: ");

            switch (type)
            {
                case "fish":
                    // retrieve fish details from user
                    waterType = retrieveString("Enter pet Water type: ");
                    name = retrieveString("Enter pet Name: ");
                    breed = retrieveString("Enter pet Breed: ");
                    colour = retrieveString("Enter pet Colour: ");

                    do
                    {
                        tryAgain = false;

                        gender = retrieveString("Enter pet Gender: ");

                        switch (gender)
                        {
                            case "m":
                            case "male":
                            case "f":
                            case "female":
                                break;
                            default:
                                System.out.println("Please enter M/MALE or F/FEMALE:");
                                tryAgain = true;
                        }

                    } while (tryAgain == true);

                    age = retrieveInt("Enter pet Age: ");

                    System.out.println("Would you like to add an owner to this pet now?: ");

                    answer = retrieveString("yes/y or no/n: ");
                    do
                    {
                        tryAgain = false;
                        switch (answer)
                        {
                            case "y":
                            case "yes":

                                tryAgain = false;

                                String ownerName = retrieveString("\nEnter your name: ");
                                String email = retrieveString("\nEnter your email: ");
                                String telephone = retrieveString("\nEnter your telephone: ");
                                String homeAddress = retrieveString("\nEnter your home address: ");

                                // make new Owner
                                Owner ownerYes = new Owner(ownerName, email, telephone, homeAddress);

                                // make a new Fish
                                Fish tempFishWithOwner = new Fish(type, name, breed, colour,
                                        gender, age, ownerYes, waterType);

                                // add Fish and Owner to fishes LinkedHashSet
                                petsFromTxt.getFishes().put(tempFishWithOwner, ownerYes);
                                pets.getPets().put(tempFishWithOwner, ownerYes);

                                // write to text file
                                Fish.writeFishDetailsToTextFile(petsFromTxt.getFishes());

                                // write to bin file
                                Fish.writeFishDetailsToBinaryFile(pets.getFishes());
                                // write as Object to bin file
                                //Fish.writeFishOS(Pets.getFishes());
                                Pet.writePetOS(pets.getPets());

                                exit = true;

                                break;
                            case "n":
                            case "no":

                                // make a new Fish
                                Fish tempFishNoOwner = new Fish(type, name, breed, colour,
                                        gender, age, waterType);

                                // add Fish to fishes LinkedHashSet
                                petsFromTxt.getFishes().put(tempFishNoOwner, new Owner());
                                pets.getPets().put(tempFishNoOwner, new Owner());

                                // write to text file
                                Fish.writeFishDetailsToTextFile(petsFromTxt.getFishes());
                                // write to bin file
                                Fish.writeFishDetailsToBinaryFile(pets.getFishes());
                                // write as Object to bin file
                                //Fish.writeFishOS(Pets.getFishes());
                                Pet.writePetOS(pets.getPets());

                                exit = true;

                                break;
                            default:
                                System.out.println("Enter only yes/y or no/n!");
                                tryAgain = true;
                                break;
                        }
                    } while (tryAgain == true);

                    tryAgain = false;

                    break;
                case "bird":
                    // retrieve bird details from user
                    wingSpan = retrieveDouble("Enter pet Wingspan: ");
                    canFly = retrieveBoolean("Enter pet can fly?: ");
                    name = retrieveString("Enter pet Name: ");
                    breed = retrieveString("Enter pet Breed: ");
                    colour = retrieveString("Enter pet Colour: ");
                    do
                    {
                        tryAgain = false;

                        gender = retrieveString("Enter pet Gender: ");

                        switch (gender)
                        {
                            case "m":
                            case "male":
                            case "f":
                            case "female":
                                break;
                            default:
                                System.out.println("Please enter M/MALE or F/FEMALE:");
                                tryAgain = true;
                        }

                    } while (tryAgain == true);
                    age = retrieveInt("Enter pet Age: ");
                    System.out.println("Would you like to add an owner to this pet now?: ");

                    answer = retrieveString("yes/y or no/n: ");
                    do
                    {
                        tryAgain = false;
                        switch (answer)
                        {
                            case "y":
                            case "yes":

                                tryAgain = false;

                                String ownerName = retrieveString("\nEnter your name: ");
                                String email = retrieveString("\nEnter your email: ");
                                String telephone = retrieveString("\nEnter your telephone: ");
                                String homeAddress = retrieveString("\nEnter your home address: ");

                                // make new Owner
                                Owner ownerYes = new Owner(ownerName, email, telephone, homeAddress);

                                // make a new Bird
                                Bird tempBirdWithOwner = new Bird(type, name, breed,
                                        colour, gender, age, ownerYes, wingSpan, canFly);

                                // add Bird and Owner to birds LinkedHashSet
                                petsFromTxt.getBirds().put(tempBirdWithOwner, ownerYes);
                                pets.getPets().put(tempBirdWithOwner, ownerYes);

                                // write to text file
                                Bird.writeBirdDetailsToTextFile(petsFromTxt.getBirds());
                                // write to bin file
                                Bird.writeBirdDetailsToBinaryFile(pets.getBirds());
                                // write as Object to bin file
                                //Bird.writeBirdOS(Pets.getBirds());
                                Pet.writePetOS(pets.getPets());

                                exit = true;

                                break;
                            case "n":
                            case "no":

                                // make a new Bird
                                Bird tempBirdNoOwner = new Bird(type, name, breed,
                                        colour, gender, age, wingSpan, canFly);

                                // add Bird and Owner to birds LinkedHashSet
                                petsFromTxt.getBirds().put(tempBirdNoOwner, new Owner());
                                pets.getPets().put(tempBirdNoOwner, new Owner());

                                // write to text file
                                Bird.writeBirdDetailsToTextFile(petsFromTxt.getBirds());
                                // write to bin file
                                Bird.writeBirdDetailsToBinaryFile(pets.getBirds());
                                // write as Object to bin file
                                //Bird.writeBirdOS(Pets.getBirds());
                                Pet.writePetOS(pets.getPets());

                                exit = true;

                                break;
                            default:
                                System.out.println("Enter only yes/y or no/n!");
                                tryAgain = true;
                                break;
                        }
                    } while (tryAgain == true);

                    tryAgain = false;

                    break;
                default:
                    if (type.matches(onlyLettersAndSpaceRegX))
                    {
                        // retrieve mammal details from user
                        neutered = retrieveBoolean("Enter pet neutered?: ");
                        name = retrieveString("Enter pet Name: ");
                        breed = retrieveString("Enter pet Breed: ");
                        colour = retrieveString("Enter pet Colour: ");
                        do
                        {
                            tryAgain = false;

                            gender = retrieveString("Enter pet Gender: ");

                            switch (gender)
                            {
                                case "m":
                                case "male":
                                case "f":
                                case "female":
                                    break;
                                default:
                                    System.out.println("Please enter M/MALE or F/FEMALE:");
                                    tryAgain = true;
                            }

                        } while (tryAgain == true);
                        age = retrieveInt("Enter pet Age: ");

                        System.out.println("Would you like to add an owner to this pet now?: ");

                        answer = retrieveString("yes/y or no/n: ");
                        do
                        {
                            tryAgain = false;
                            switch (answer)
                            {
                                case "y":
                                case "yes":

                                    tryAgain = false;

                                    String ownerName = retrieveString("\nEnter your name: ");
                                    String email = retrieveString("\nEnter your email: ");
                                    String telephone = retrieveString("\nEnter your telephone: ");
                                    String homeAddress = retrieveString("\nEnter your home address: ");

                                    // make new Owner
                                    Owner ownerYes = new Owner(ownerName, email, telephone, homeAddress);

                                    // make a new Mammal
                                    Mammal tempMammalWithOwner = new Mammal(type, name, breed,
                                            colour, gender, age, ownerYes, neutered);

                                    // add Mammal and Owner to mammals LinkedHashSet
                                    petsFromTxt.getMammals().put(tempMammalWithOwner, ownerYes);
                                    pets.getPets().put(tempMammalWithOwner, ownerYes);

                                    // write to text file
                                    Mammal.writeMammalDetailsToTextFile(petsFromTxt.getMammals());
                                    // write to bin file
                                    Mammal.writeMammalDetailsToBinaryFile(pets.getMammals());
                                    // write as Object to bin file
                                    //Mammal.writeMammalOS(Pets.getMammals());
                                    Pet.writePetOS(pets.getPets());

                                    exit = true;

                                    break;
                                case "n":
                                case "no":

                                    // make a new Mammal
                                    Mammal tempMammalNoOwner = new Mammal(type, name, breed,
                                            colour, gender, age, neutered);

                                    // add Mammal and Owner to mammals LinkedHashSet
                                    petsFromTxt.getMammals().put(tempMammalNoOwner, new Owner());
                                    pets.getPets().put(tempMammalNoOwner, new Owner());

                                    // write to text file
                                    Mammal.writeMammalDetailsToTextFile(petsFromTxt.getMammals());
                                    // write to bin file
                                    Mammal.writeMammalDetailsToBinaryFile(pets.getMammals());
                                    // write as Object to bin file
                                    //Mammal.writeMammalOS(Pets.getMammals());
                                    Pet.writePetOS(pets.getPets());

                                    exit = true;

                                    break;
                                default:
                                    System.out.println("Enter only yes/y or no/n!");
                                    tryAgain = true;
                                    break;
                            }
                        } while (tryAgain == true);

                        break;
                    } else
                    {
                        System.out.println("Please only enter letters, no more than 20!");
                        tryAgain = true;
                    }
                    break;
            }

        } while (tryAgain == true && exit == false);
        System.out.println("\n");

    }

    private static void editPet()
    {
        Set set = pets.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext())
        {
            Map.Entry entry = (Map.Entry) iterator.next();
            System.out.print("key: " + entry.getKey() + " & Value: ");
            System.out.println(entry.getValue());

        }
    }

    private static void addOwner()
    {

        String waterType = null;
        double wingSpan = 0;
        boolean canFly = false;
        boolean neutered = false;
        String name = null;
        String breed = null;
        String colour = null;
        String gender = null;
        int age = 0;
        String type;
        String petId = null;

        // only letters for a word with size 2 to 20 letters
        String onlyLettersRegX = "[a-z]{2,20}";

        boolean tryAgain = false;
        System.out.println("To add an owner to a pet we need the pet details:");

        do
        {
            tryAgain = false;
            type = retrieveString("\nEnter pet Type. Mammal, Fish or Bird: ");

            switch (type)
            {
                case "fish":
                    // retrieve fish details from user
                    System.out.println(type.hashCode());

                    waterType = retrieveString("Enter pet Water type: ");
                    name = retrieveString("Enter pet Name: ");
                    breed = retrieveString("Enter pet Breed: ");
                    colour = retrieveString("Enter pet Colour: ");
                    do
                    {
                        tryAgain = false;

                        gender = retrieveString("Enter pet Gender: ");

                        switch (gender)
                        {
                            case "m":
                            case "male":
                            case "f":
                            case "female":
                                break;
                            default:
                                System.out.println("Please enter M/MALE or F/FEMALE:");
                                tryAgain = true;
                        }

                    } while (tryAgain == true);
                    age = retrieveInt("Enter pet Age: ");

//                    Fish tempFish = new Fish(type, name, breed, colour, gender, age, waterType);
//                    Map<Fish, Owner> tempMap = new LinkedHashMap<>();
//                    tempMap.put(tempFish, new Owner());
                    for (Iterator<Object> it = pets.keySet().iterator(); it.hasNext();)
                    {

                        Fish fishO = (Fish) it.next();
                        Bird birdO = (Bird) it.next();
                        Mammal mammalO = (Mammal) it.next();

                        System.out.println(type.hashCode());

                        System.out.println("Type hashCode for fish is " + fishO.getType().hashCode());
                        System.out.println("*********************************************");
                        System.out.println("Type hashCode for fish is " + fishO.getType().hashCode());
                        System.out.println("id hashCode for fish is " + fishO.getPetId().hashCode());
                        System.out.println("name hashCode for fish is " + fishO.getName().hashCode());
                        System.out.println("breed hashCode for fish is " + fishO.getBreed().hashCode());
                        System.out.println("gender hashCode for fish is " + fishO.getGender().hashCode());
                        System.out.println("colour hashCode for fish is " + fishO.getColour().hashCode());
                        System.out.println("*********************************************");
                        System.out.println("Type hashCode for bird is " + birdO.getType().hashCode());
                        System.out.println("id hashCode for bird is " + birdO.getPetId().hashCode());
                        System.out.println("name hashCode for bird is " + birdO.getName().hashCode());
                        System.out.println("breed hashCode for bird is " + birdO.getBreed().hashCode());
                        System.out.println("gender hashCode for bird is " + birdO.getGender().hashCode());
                        System.out.println("colour hashCode for bird is " + birdO.getColour().hashCode());
                        System.out.println("*********************************************");
                        System.out.println("Type hashCode for mammal is " + mammalO.getType().hashCode());
                        System.out.println("id hashCode for mammal is " + mammalO.getPetId().hashCode());
                        System.out.println("name hashCode for mammal is " + mammalO.getName().hashCode());
                        System.out.println("breed hashCode for mammal is " + mammalO.getBreed().hashCode());
                        System.out.println("gender hashCode for mammal is " + mammalO.getGender().hashCode());
                        System.out.println("colour hashCode for mammal is " + mammalO.getColour().hashCode());
                        System.out.println("");

                        if (fishO.getType().hashCode() == type.hashCode())
                        {
                            if (fishO.getWaterType().equals(waterType) && fishO.getName().equals(name)
                                    && fishO.getBreed().equals(breed) && fishO.getColour().equals(colour)
                                    && fishO.getGender().equals(gender) && fishO.getAge() == age)
                            {
                                petId = fishO.getPetId();
                                System.out.println("Thank you... Pet was found!");

                            }

                        } else
                        {
                            it.next();
                        }

                    }

                    String ownerName = retrieveString("\nEnter your name: ");
                    String email = retrieveString("\nEnter your email: ");
                    String telephone = retrieveString("\nEnter your telephone: ");
                    String homeAddress = retrieveString("\nEnter your home address: ");

                    // make new Owner
                    Owner ownerYes = new Owner(ownerName, email, telephone, homeAddress);

                    for (Iterator<Object> it = pets.keySet().iterator(); it.hasNext();)
                    {
                        Fish fishO = (Fish) it.next();
                        System.out.println(it);
                        System.out.println(it.toString());
                        System.out.println(fishO);
                        System.out.println(fishO.toString());

                        if (fishO.getPetId().equals(petId))
                        {
                            pets.replace(it, ownerYes);
                        }

                    }

                    System.out.println(pets.entrySet());

//                    pets.getPets().forEach((key, value) ->
//                    {
//
//                        if (tempMap.get(key).getName().equals(pets.getPets().get(key).getName()))
//                        {
//                            System.out.println("yes");
//                        }
//
//                    });
//                    for (Object tempPet : pets.entrySet())
//                    {
//                        System.out.println(tempPet.getClass().getName());
//                        System.out.println(Fish.class.getName());
//                        System.out.println(tempPet);
//                        if (tempPet.getClass().getName().equals(Fish.class.getName()))
//                        {
//                            System.out.println("Yes");
//
//                        }
//                    }
//                    Set set = pets.entrySet();
//                    Iterator iterator = set.iterator();
//                    while (iterator.hasNext())
//                    {
//                        Map.Entry entry = (Map.Entry) iterator.next();
//
//                        System.out.println("key: " + tempMap.get(entry.getKey()));
//
//                        System.out.println("key: " + entry.getKey() + " & Value: ");
//                        System.out.println(entry.getValue());
//5
//                    }
//                    pets.getPets().forEach((Pet key, Owner value) ->
//                    {
//                        if (tempMap.containsKey(key.getAge()))// && tempFish.getName().equals(key.getName())
//                                //&& tempFish.getBreed().equals(key.getBreed()) && tempFish.getColour().equals(key.getColour())
//                                //&& tempFish.getGender().equals(key.getGender()) && tempFish.getAge() == key.getAge())
//                        {
//                            System.out.println(key.getPetId());
//                        }
//+ "," + key.getOwnerId() + "," + key.getType()
//                                    + "," + key.getWaterType() + "," + key.getName() + ","
//                                    + key.getBreed() + "," + key.getColour() + "," + key.getGender()
//                                    + "," + key.getDateRegistered() + ","
//                                    + key.getAge() + " | " + value.getOwnerId() + "," + value.getName() + "," + value.getEmail() + ","
//                                    + value.getTelephone() + "," + value.getHomeAddress() + "\n");
                    // });
//                    Set set = pets.entrySet();
//                    Iterator iterator = set.iterator();
//                    while (iterator.hasNext())
//                    {
//                        Map.Entry entry = (Map.Entry) iterator.next();
//                        if (entry.equals())
//                        {
//                            System.out.println(entry.getKey());
//                        }
////                        Map.Entry entry = (Map.Entry) iterator.next();
////                        System.out.print("key: " + entry.getKey() + " & Value: ");
////                        System.out.println(entry.getValue());
//
//                    }
                    break;
                case "bird":
                    System.out.println(type.hashCode());

                    // retrieve bird details from user
                    wingSpan = retrieveDouble("Enter pet Wingspan: ");
                    canFly = retrieveBoolean("Enter pet can fly?: ");
                    name = retrieveString("Enter pet Name: ");
                    breed = retrieveString("Enter pet Breed: ");
                    colour = retrieveString("Enter pet Colour: ");
                    gender = retrieveString("Enter pet Gender: ");
                    age = retrieveInt("Enter pet Age: ");

                    for (Iterator<Object> it = pets.keySet().iterator(); it.hasNext();)
                    {

                        Fish fishO = (Fish) it.next();
                        Bird birdO = (Bird) it.next();
                        Mammal mammalO = (Mammal) it.next();

                        System.out.println(type.hashCode());
                        System.out.println(name.hashCode());
                        System.out.println(breed.hashCode());
                        System.out.println(gender.hashCode());
                        System.out.println(colour.hashCode());

                        System.out.println("Type hashCode for bird is " + birdO.getType().hashCode());
                        System.out.println("*********************************************");
                        System.out.println("Type hashCode for fish is " + fishO.getType().hashCode());
                        System.out.println("id hashCode for fish is " + fishO.getPetId().hashCode());
                        System.out.println("name hashCode for fish is " + fishO.getName().hashCode());
                        System.out.println("breed hashCode for fish is " + fishO.getBreed().hashCode());
                        System.out.println("gender hashCode for fish is " + fishO.getGender().hashCode());
                        System.out.println("colour hashCode for fish is " + fishO.getColour().hashCode());
                        System.out.println("*********************************************");
                        System.out.println("Type hashCode for bird is " + birdO.getType().hashCode());
                        System.out.println("id hashCode for bird is " + birdO.getPetId().hashCode());
                        System.out.println("name hashCode for bird is " + birdO.getName().hashCode());
                        System.out.println("breed hashCode for bird is " + birdO.getBreed().hashCode());
                        System.out.println("gender hashCode for bird is " + birdO.getGender().hashCode());
                        System.out.println("colour hashCode for bird is " + birdO.getColour().hashCode());
                        System.out.println("*********************************************");
                        System.out.println("Type hashCode for mammal is " + mammalO.getType().hashCode());
                        System.out.println("id hashCode for mammal is " + mammalO.getPetId().hashCode());
                        System.out.println("name hashCode for mammal is " + mammalO.getName().hashCode());
                        System.out.println("breed hashCode for mammal is " + mammalO.getBreed().hashCode());
                        System.out.println("gender hashCode for mammal is " + mammalO.getGender().hashCode());
                        System.out.println("colour hashCode for mammal is " + mammalO.getColour().hashCode());
                        System.out.println("");

                        if (birdO.getName().hashCode() == name.hashCode())
                        {
                            if (birdO.getBreed().hashCode() == breed.hashCode())
                            {
                                System.out.println("inside getBreed.hashCode block!!!");
                                if (birdO.getWingSpan() == wingSpan && birdO.canFly() == canFly && birdO.getName().equals(name)
                                        && birdO.getBreed().equals(breed) && birdO.getColour().equals(colour)
                                        && birdO.getGender().equals(gender) && birdO.getAge() == age)
                                {
                                    System.out.println("name match for bird " + fishO.getPetId());

                                }
                            }

                        } else
                        {
                            it.next();
                        }
                    }
                    break;
                default:
                    if (type.matches(onlyLettersRegX))
                    {
                        System.out.println(type.hashCode());

                        // retrieve mammal details from user
                        neutered = retrieveBoolean("Enter pet neutered?: ");
                        name = retrieveString("Enter pet Name: ");
                        breed = retrieveString("Enter pet Breed: ");
                        colour = retrieveString("Enter pet Colour: ");
                        gender = retrieveString("Enter pet Gender: ");
                        age = retrieveInt("Enter pet Age: ");

                        for (Iterator<Object> it = pets.keySet().iterator(); it.hasNext();)
                        {

                            Fish fishO = (Fish) it.next();
                            Bird birdO = (Bird) it.next();
                            Mammal mammalO = (Mammal) it.next();

                            System.out.println(type.hashCode());

                            System.out.println("Type hashCode for bird is " + mammalO.getType().hashCode());
                            System.out.println("*********************************************");
                            System.out.println("Type hashCode for fish is " + fishO.getType().hashCode());
                            System.out.println("id hashCode for fish is " + fishO.getPetId().hashCode());
                            System.out.println("name hashCode for fish is " + fishO.getName().hashCode());
                            System.out.println("breed hashCode for fish is " + fishO.getBreed().hashCode());
                            System.out.println("gender hashCode for fish is " + fishO.getGender().hashCode());
                            System.out.println("colour hashCode for fish is " + fishO.getColour().hashCode());
                            System.out.println("*********************************************");
                            System.out.println("Type hashCode for bird is " + birdO.getType().hashCode());
                            System.out.println("id hashCode for bird is " + birdO.getPetId().hashCode());
                            System.out.println("name hashCode for bird is " + birdO.getName().hashCode());
                            System.out.println("breed hashCode for bird is " + birdO.getBreed().hashCode());
                            System.out.println("gender hashCode for bird is " + birdO.getGender().hashCode());
                            System.out.println("colour hashCode for bird is " + birdO.getColour().hashCode());
                            System.out.println("*********************************************");
                            System.out.println("Type hashCode for mammal is " + mammalO.getType().hashCode());
                            System.out.println("id hashCode for mammal is " + mammalO.getPetId().hashCode());
                            System.out.println("name hashCode for mammal is " + mammalO.getName().hashCode());
                            System.out.println("breed hashCode for mammal is " + mammalO.getBreed().hashCode());
                            System.out.println("gender hashCode for mammal is " + mammalO.getGender().hashCode());
                            System.out.println("colour hashCode for mammal is " + mammalO.getColour().hashCode());
                            System.out.println("");

                            if (mammalO.getType().hashCode() == type.hashCode())
                            {
                                if (mammalO.isNeutered() == neutered && mammalO.getName().equals(name)
                                        && mammalO.getBreed().equals(breed) && mammalO.getColour().equals(colour)
                                        && mammalO.getGender().equals(gender) && mammalO.getAge() == age)
                                {
                                    System.out.println("name match for mammal " + fishO.getPetId());

                                }

                            } else
                            {
                                it.next();
                            }
                        }

                        break;
                    } else
                    {
                        System.out.println("Please only enter letters, no more than 20!");
                        tryAgain = true;
                    }
                    break;
            }
        } while (tryAgain == true);

        System.out.println("Thank you...");

        String ownerName = retrieveString("\nEnter your name: ");
        String email = retrieveString("\nEnter your email: ");
        String telephone = retrieveString("\nEnter your telephone: ");
        String homeAddress = retrieveString("\nEnter your home address: ");

        // make new Owner
        Owner ownerYes = new Owner(ownerName, email, telephone, homeAddress);

        // set owner to pet
        // make a new Bird
        Bird tempBirdWithOwner = new Bird(type, name, breed,
                colour, gender, age, ownerYes, wingSpan, canFly);

        // add Bird and Owner to birds LinkedHashSet
        petsFromTxt.getBirds().put(tempBirdWithOwner, ownerYes);
        pets.getPets().put(tempBirdWithOwner, ownerYes);

        // write to text file
        Bird.writeBirdDetailsToTextFile(petsFromTxt.getBirds());
        // write to bin file
        Bird.writeBirdDetailsToBinaryFile(pets.getBirds());
        // write as Object to bin file
        //Bird.writeBirdOS(Pets.getBirds());
        Pet.writePetOS(pets.getPets());

    }

//    private static void readPetDetailsFromBinaryFile()
//    {
//        try (DataInputStream petDetailsBinFile = new DataInputStream(new BufferedInputStream(new FileInputStream("petDetailsFile.bin"))))
//        {
//            boolean eof = false;
//            while (eof == false)
//            {
//                try
//                {
//                    Map<String, Integer> exits = new LinkedHashMap();
//                    int locID = petDetailsBinFile.readInt();
//                    String description = locationsFile.readUTF();
//                    System.out.println("Read location" + locID + " : " + description);
//                    int numExits = locationsFile.readInt();
//                    System.out.println("Found " + numExits + " exits");
//                    for (int i = 0; i < numExits; i++)
//                    {
//                        String direction = locationsFile.readUTF();
//                        int destination = locationsFile.readInt();
//                        exits.put(direction, destination);
//                        System.out.println("\t\t" + direction + ", " + destination);
//                    }
//                    locations.put(locID, new Location(locID, description, exits));
//                } catch (EOFException e)
//                {
//                    eof = true;
//                }
//            }
//        } catch (IOException e)
//        {
//            System.out.println("IO Exception");
//        }
//    }
    private static void deletePet()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void printPetDetails()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
