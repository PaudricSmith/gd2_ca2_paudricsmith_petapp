// D00215637 Paudric Smith
package gd2.gd2_ca2_paudricsmith_petapp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Bird extends Pet
{

    private double wingSpan;
    private boolean canFly;

    public Bird(String type, String name, String breed, String colour,
            String gender, int age, Owner owner, double wingSpan, boolean canFly)
    {
        super(type, name, breed, colour, gender, age, owner);
        this.wingSpan = wingSpan;
        this.canFly = canFly;
    }

    public Bird(String type, String name, String breed, String colour,
            String gender, int age, double wingSpan, boolean canFly)
    {
        super(type, name, breed, colour, gender, age, new Owner());
        this.wingSpan = wingSpan;
        this.canFly = canFly;
    }

    public double getWingSpan()
    {
        return wingSpan;
    }

    public boolean canFly()
    {
        return canFly;
    }

    public void setWingSpan(double wingSpan)
    {
        this.wingSpan = wingSpan;
    }

    public void setCanFly(boolean canFly)
    {
        this.canFly = canFly;
    }

    public static void writeBirdDetailsToTextFile(Map<Bird, Owner> birds)
    {
        // write Bird details to file
        try (FileWriter petDetailsFile = new FileWriter("petDetailsFile.txt", true))
        {
            birds.forEach((key, value) ->
            {
                try
                {
                    petDetailsFile.write(key.getPetId() + "," + key.getOwnerId() + "," + key.getType()
                            + "," + key.getWingSpan() + "," + key.canFly() + "," + key.getName() + ","
                            + key.getBreed() + "," + key.getColour() + "," + key.getGender()
                            + "," + key.getDateRegistered() + ","
                            + key.getAge() + "," + value.getOwnerId() + "," + value.getName() + "," + value.getEmail() + ","
                            + value.getTelephone() + "," + value.getHomeAddress() + ",");
                } catch (IOException e)
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
                }

            });
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void writeBirdDetailsToBinaryFile(Map<Bird, Owner> birds)
    {
        try (DataOutputStream petDetailsBinFile = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(("petDetailsFile.bin")))))
        {

            birds.forEach((key, value) ->
            {
                try
                {
                    petDetailsBinFile.writeUTF(key.getPetId() + "," + key.getOwnerId() + "," + key.getType()
                            + "," + key.canFly() + "," + key.getWingSpan() + "," + key.getName() + ","
                            + key.getBreed() + "," + key.getColour() + "," + key.getGender()
                            + "," + key.getDateRegistered() + ","
                            + key.getAge() + " | " + value.getOwnerId() + "," + value.getName() + "," + value.getEmail() + ","
                            + value.getTelephone() + "," + value.getHomeAddress() + "\n");
                } catch (IOException e)
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
                }

            });
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    public static void writeBirdOS(Map<Bird, Owner> birds)
    {
        try (ObjectOutputStream birdDetailsFileOS = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("birdDetailsFileOS.ser"))))
        {
            birdDetailsFileOS.writeObject(birds);

        } catch (IOException e)
        {
            System.out.println("IO Exception");
        }
    }

    public static void readBirdOS() throws IOException
    {

        try (ObjectInputStream birdDetailsFileOS = new ObjectInputStream(new BufferedInputStream(new FileInputStream("birdDetailsFileOS.ser"))))
        {
            Map<Bird, Owner> birds;
            birds = (LinkedHashMap) birdDetailsFileOS.readObject();
            Pets.getBirds().putAll(birds);
            //Pets.getPets().putAll(Pets.getBirds());

        } catch (InvalidClassException ice)
        {
            System.out.println("Invalid Class");
        } catch (FileNotFoundException | ClassNotFoundException e)
        {
            Logger.getLogger(Bird.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 19 * hash + (int) (Double.doubleToLongBits(this.wingSpan) ^ (Double.doubleToLongBits(this.wingSpan) >>> 32));
        hash = 19 * hash + (this.canFly ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Bird other = (Bird) obj;
        if (Double.doubleToLongBits(this.wingSpan) != Double.doubleToLongBits(other.wingSpan))
        {
            return false;
        }
        if (this.canFly != other.canFly)
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "Bird{" + super.toString() + "wingSpan=" + wingSpan + ", canFly=" + canFly + '}';
    }

}
