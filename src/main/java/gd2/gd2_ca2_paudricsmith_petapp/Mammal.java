// D00215637 Paudric Smith
package gd2.gd2_ca2_paudricsmith_petapp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Mammal extends Pet
{

    private boolean neutered;

    public Mammal(String type, String name, String breed, String colour, String gender,
            int age, Owner owner, boolean neutered)
    {
        super(type, name, breed, colour, gender, age, owner);
        this.neutered = neutered;
    }

    public Mammal(String type, String name, String breed, String colour, String gender,
            int age, boolean neutered)
    {
        super(type, name, breed, colour, gender, age, new Owner());
        this.neutered = neutered;
    }

    public boolean isNeutered()
    {
        return neutered;
    }

    public void setNeutered(boolean neutered)
    {
        this.neutered = neutered;
    }

    public static void writeMammalDetailsToTextFile(Map<Mammal, Owner> mammals)
    {
        // write Mammal details to file
        try (FileWriter petDetailsFile = new FileWriter("petDetailsFile.txt", true))
        {
            mammals.forEach((key, value) ->
            {
                try
                {
                    petDetailsFile.write(key.getPetId() + "," + key.getOwnerId() + "," + key.getType()
                            + "," + key.isNeutered() + "," + key.getName() + ","
                            + key.getBreed() + "," + key.getColour() + "," + key.getGender()
                            + "," + key.getDateRegistered() + ","
                            + key.getAge() + "," + value.getOwnerId() + "," + value.getName() + ","
                            + value.getEmail() + "," + value.getTelephone() + "," + value.getHomeAddress() + "," + "\n");
                } catch (IOException e)
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
                }

            });
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void writeMammalDetailsToBinaryFile(Map<Mammal, Owner> mammals)
    {
        try (DataOutputStream petDetailsBinFile = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(("petDetailsFile.bin")))))
        {

            mammals.forEach((key, value) ->
            {
                try
                {
                    petDetailsBinFile.writeUTF(key.getPetId() + "," + key.getOwnerId() + ","
                            + key.getType() + "," + key.isNeutered() + "," + key.getName() + ","
                            + key.getBreed() + "," + key.getColour() + "," + key.getGender()
                            + "," + key.getDateRegistered() + "," + key.getAge() + " | " + value.getOwnerId()
                            + "," + value.getName() + "," + value.getEmail() + ","
                            + value.getTelephone() + "," + value.getHomeAddress() + "\n");
                } catch (IOException e)
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
                }

            });
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    public static void writeMammalOS(Map<Mammal, Owner> mammals)
    {
        try (ObjectOutputStream mammalDetailsFileOS = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("mammalDetailsFileOS.ser"))))
        {
            mammalDetailsFileOS.writeObject(mammals);

        } catch (IOException e)
        {
            System.out.println("IO Exception");
        }
    }

    public static void readMammalOS() throws IOException
    {

        try (ObjectInputStream mammalDetailsFileOS = new ObjectInputStream(new BufferedInputStream(new FileInputStream("mammalDetailsFileOS.ser"))))
        {
            Map<Mammal, Owner> mammals;
            mammals = (LinkedHashMap) mammalDetailsFileOS.readObject();
            Pets.getMammals().putAll(mammals);
            //Pets.getPets().putAll(Pets.getFishes());

        } catch (InvalidClassException ice)
        {
            System.out.println("Invalid Class");
        } catch (FileNotFoundException | ClassNotFoundException ex)
        {
            Logger.getLogger(Mammal.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void readFromBinary() throws IOException
    {

        try (ObjectInputStream mammalDetailsFileOS = new ObjectInputStream(new BufferedInputStream(new FileInputStream("mammalDetailsFileOS.ser"))))
        {
            Map<Mammal, Owner> mammals;
            mammals = (LinkedHashMap) mammalDetailsFileOS.readObject();
            Pets.getMammals().putAll(mammals);
            //Pets.getPets().putAll(Pets.getFishes());

        } catch (InvalidClassException ice)
        {
            System.out.println("Invalid Class");
        } catch (FileNotFoundException | ClassNotFoundException ex)
        {
            Logger.getLogger(Mammal.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 47 * hash + (this.neutered ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Mammal other = (Mammal) obj;
        if (this.neutered != other.neutered)
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "Mammal{" + super.toString() + "neutered=" + neutered + '}';
    }

}
