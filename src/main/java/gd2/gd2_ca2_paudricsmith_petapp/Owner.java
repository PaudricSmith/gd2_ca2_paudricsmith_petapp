// D00215637 Paudric Smith
package gd2.gd2_ca2_paudricsmith_petapp;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class Owner implements Serializable
{

    private String name;
    private String ownerId;
    private String email;
    private String telephone;
    private String homeAddress;
    private UUID uniqueId;
    private static final long serialVersionUID = 12345L;

    public Owner(String name, String email, String telephone, String homeAddress)
    {
        this.uniqueId = UUID.randomUUID();
        this.name = name;

        if (this.ownerId == null)
        {
            this.ownerId = "OID" + uniqueId.toString();
        }

        this.email = email;
        this.telephone = telephone;
        this.homeAddress = homeAddress;
    }

    public Owner()
    {
        this.name = "n/n";
        this.email = "n/e";
        this.telephone = "n/t";
        this.homeAddress = "n/a";
        this.ownerId = "n/i";
        this.uniqueId = null;

    }

    public static void addOwner(Owner owner)
    {
        writeOwnerDetailsToTextFile(owner);
    }

    private static void writeOwnerDetailsToTextFile(Owner owner)
    {
        try (FileWriter petDetailsFile = new FileWriter("petDetailsFile.txt", true))
        {
            petDetailsFile.write(owner.getName() + "," + owner.getEmail()
                    + "," + owner.getTelephone() + "," + owner.getHomeAddress()
                    + "," + owner.getOwnerId() + "\n");

        } catch (IOException e)
        {
            System.out.println("IO Exception!");
        }
    }

    public String getName()
    {
        return name;
    }

    public String getOwnerId()
    {
        return ownerId;
    }

    public String getEmail()
    {
        return email;
    }

    public String getTelephone()
    {
        return telephone;
    }

    public String getHomeAddress()
    {
        return homeAddress;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setOwnerId(String ownerId)
    {
        this.ownerId = ownerId;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }

    public void setHomeAddress(String homeAddress)
    {
        this.homeAddress = homeAddress;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.name);
        hash = 67 * hash + Objects.hashCode(this.ownerId);
        hash = 67 * hash + Objects.hashCode(this.email);
        hash = 67 * hash + Objects.hashCode(this.telephone);
        hash = 67 * hash + Objects.hashCode(this.homeAddress);
        hash = 67 * hash + Objects.hashCode(this.uniqueId);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Owner other = (Owner) obj;
        if (!Objects.equals(this.name, other.name))
        {
            return false;
        }
        if (!Objects.equals(this.ownerId, other.ownerId))
        {
            return false;
        }
        if (!Objects.equals(this.email, other.email))
        {
            return false;
        }
        if (!Objects.equals(this.telephone, other.telephone))
        {
            return false;
        }
        if (!Objects.equals(this.homeAddress, other.homeAddress))
        {
            return false;
        }
        if (!Objects.equals(this.uniqueId, other.uniqueId))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return " Owner: " + "name=" + name + ", ownerId=" + ownerId + ", email=" + email + ", telephone=" + telephone + ", homeAddress=" + homeAddress;
    }

}
