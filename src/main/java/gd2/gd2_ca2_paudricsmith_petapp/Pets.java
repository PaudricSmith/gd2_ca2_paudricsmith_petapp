// D00215637 Paudric Smith
package gd2.gd2_ca2_paudricsmith_petapp;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Pets implements Map<Object, Owner>
{

    private static Map<Object, Owner> pets = new LinkedHashMap<>();
    private static Map<Fish, Owner> fishes = new LinkedHashMap<>();
    private static Map<Bird, Owner> birds = new LinkedHashMap<>();
    private static Map<Mammal, Owner> mammals = new LinkedHashMap<>();
    private static Map<Pet, Owner> petsFromTxt = new LinkedHashMap<>();

    public static Map<Object, Owner> getPets()
    {
        return pets;
    }

    public static Map<Fish, Owner> getFishes()
    {
        return fishes;
    }

    public static Map<Bird, Owner> getBirds()
    {
        return birds;
    }

    public static Map<Mammal, Owner> getMammals()
    {
        return mammals;
    }

    public static Map<Pet, Owner> getPetsFromTxt()
    {
        return petsFromTxt;
    }

    @Override
    public String toString()
    {
        return "" + entrySet();
    }

    @Override
    public int size()
    {
        return pets.size();
    }

    @Override
    public boolean isEmpty()
    {
        return pets.isEmpty();
    }

    @Override
    public boolean containsKey(Object key)
    {
        return pets.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value)
    {
        return pets.containsValue(value);
    }

    @Override
    public Owner get(Object key)
    {
        return pets.get(key);
    }

    @Override
    public Owner put(Object key, Owner value)
    {
        return pets.put(key, value);
    }

    @Override
    public Owner remove(Object key)
    {
        return pets.remove(key);
    }

    @Override
    public void putAll(Map<? extends Object, ? extends Owner> m)
    {
        pets.putAll(m);
    }

    @Override
    public void clear()
    {
        pets.clear();
    }

    @Override
    public Set<Object> keySet()
    {
        return pets.keySet();
    }

    @Override
    public Collection<Owner> values()
    {
        return pets.values();
    }

    @Override
    public Set<Entry<Object, Owner>> entrySet()
    {
        return pets.entrySet();
    }

}
