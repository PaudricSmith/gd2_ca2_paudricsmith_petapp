// D00215637 Paudric Smith
package gd2.gd2_ca2_paudricsmith_petapp;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Pet implements Serializable
{

    private static final long serialVersionUID = 12345L;
    Date date;
    DateFormat formatter;
    private String dateRegistered;
    private String type;
    private String name;
    private String breed;
    private String colour;
    private String gender;
    private final UUID uniqueId;
    private String petId;
    private String ownerId;
    private int age;

    public Pet(String type, String name, String breed, String colour, String gender,
            int age, Owner owner)
    {

        this.date = new Date();
        this.formatter = new SimpleDateFormat("dd-MM-yyyy-h:mma");
        this.dateRegistered = formatter.format(date);
        this.uniqueId = UUID.randomUUID();
        this.type = type;
        this.name = name;
        this.breed = breed;
        this.colour = colour;
        this.gender = gender;
        this.petId = "PID" + uniqueId.toString();
        this.ownerId = owner.getOwnerId();
        this.age = age;
    }

//    private static void readPetDetailsFromBinaryFile()
//    {
//        try (DataInputStream locationsFile = new DataInputStream(new BufferedInputStream(new FileInputStream("locations.bin"))))
//        {
//            boolean eof = false;
//            while (eof == false)
//            {
//                try
//                {
//                    Map<String, Integer> exits = new LinkedHashMap();
//                    int locID = locationsFile.readInt();
//                    String description = locationsFile.readUTF();
//                    System.out.println("Read location" + locID + " : " + description);
//                    int numExits = locationsFile.readInt();
//                    System.out.println("Found " + numExits + " exits");
//                    for (int i = 0; i < numExits; i++)
//                    {
//                        String direction = locationsFile.readUTF();
//                        int destination = locationsFile.readInt();
//                        exits.put(direction, destination);
//                        System.out.println("\t\t" + direction + ", " + destination);
//                    }
//                    locations.put(locID, new Location(locID, description, exits));
//                } catch (EOFException e)
//                {
//                    eof = true;
//                }
//            }
//        } catch (IOException e)
//        {
//            System.out.println("IO Exception");
//        }
//    }
    static void readPetOS() throws IOException
    {

        try (ObjectInputStream petDetailsFileOS = new ObjectInputStream(new BufferedInputStream(new FileInputStream("petDetailsFileOS.ser"))))
        {

            if (petDetailsFileOS != null)
            {
                Map<Pet, Owner> pets;
                pets = (LinkedHashMap) petDetailsFileOS.readObject();
                Pets.getPets().putAll(pets);
                //Pets.getPets().putAll(Pets.getFishes());
            }

        } catch (InvalidClassException ice)
        {
            System.out.println(ice.getMessage());
        } catch (FileNotFoundException fnfe)
        {
            //not doing anything here because don't need to see it on console
        } catch (ClassNotFoundException cnfe)
        {
            System.out.println(cnfe.getMessage());
        }
    }

    public static void writePetOS(Map<Object, Owner> pets)
    {
        try (ObjectOutputStream petDetailsFileOS = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("petDetailsFileOS.ser"))))
        {
            petDetailsFileOS.writeObject(pets);

        } catch (IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }

    public UUID getUniqueId()
    {
        return uniqueId;
    }

    public String getType()
    {
        return type;
    }

    public String getName()
    {
        return name;
    }

    public String getBreed()
    {
        return breed;
    }

    public String getColour()
    {
        return colour;
    }

    public String getGender()
    {
        return gender;
    }

    public String getDateRegistered()
    {
        return dateRegistered;
    }

    public String getPetId()
    {
        return petId;
    }

    public String getOwnerId()
    {
        return ownerId;
    }

    public int getAge()
    {
        return age;
    }

    public void setOwnerId(String ownerId)
    {
        this.ownerId = ownerId;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setBreed(String breed)
    {
        this.breed = breed;
    }

    public void setColour(String colour)
    {
        this.colour = colour;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public void setDateRegistered(String dateRegistered)
    {
        this.dateRegistered = dateRegistered;
    }

    public void setPetId(String petId)
    {
        this.petId = petId;
    }

    public void setOwnerId(Owner owner)
    {
        this.ownerId = owner.getOwnerId();
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.date);
        hash = 31 * hash + Objects.hashCode(this.formatter);
        hash = 31 * hash + Objects.hashCode(this.dateRegistered);
        hash = 31 * hash + Objects.hashCode(this.type);
        hash = 31 * hash + Objects.hashCode(this.name);
        hash = 31 * hash + Objects.hashCode(this.breed);
        hash = 31 * hash + Objects.hashCode(this.colour);
        hash = 31 * hash + Objects.hashCode(this.gender);
        hash = 31 * hash + Objects.hashCode(this.uniqueId);
        hash = 31 * hash + Objects.hashCode(this.petId);
        hash = 31 * hash + Objects.hashCode(this.ownerId);
        hash = 31 * hash + this.age;
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Pet other = (Pet) obj;
        if (this.age != other.age)
        {
            return false;
        }
        if (!Objects.equals(this.dateRegistered, other.dateRegistered))
        {
            return false;
        }
        if (!Objects.equals(this.type, other.type))
        {
            return false;
        }
        if (!Objects.equals(this.name, other.name))
        {
            return false;
        }
        if (!Objects.equals(this.breed, other.breed))
        {
            return false;
        }
        if (!Objects.equals(this.colour, other.colour))
        {
            return false;
        }
        if (!Objects.equals(this.gender, other.gender))
        {
            return false;
        }
        if (!Objects.equals(this.petId, other.petId))
        {
            return false;
        }
        if (!Objects.equals(this.ownerId, other.ownerId))
        {
            return false;
        }
        if (!Objects.equals(this.date, other.date))
        {
            return false;
        }
        if (!Objects.equals(this.formatter, other.formatter))
        {
            return false;
        }
        if (!Objects.equals(this.uniqueId, other.uniqueId))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return " Pet: " + "type= " + type + ", name= " + name + ", breed= " + breed + ", colour= " + colour + ", gender= " + gender + ", dateRegistered= " + dateRegistered + ", petId= " + petId + ", age= " + age + ", ownerId= " + ownerId + "  ";
    }

}
