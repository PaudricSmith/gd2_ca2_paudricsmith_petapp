// D00215637 Paudric Smith
package gd2.gd2_ca2_paudricsmith_petapp;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Fish extends Pet
{

    private String waterType;

    public Fish(String type, String name, String breed, String colour, String gender,
            int age, Owner owner, String waterType)
    {
        super(type, name, breed, colour, gender, age, owner);
        this.waterType = waterType;
    }

    public Fish(String type, String name, String breed, String colour, String gender,
            int age, String waterType)
    {
        super(type, name, breed, colour, gender, age, new Owner());
        this.waterType = waterType;
    }

    public String getWaterType()
    {
        return waterType;
    }

    public void setWaterType(String waterType)
    {
        this.waterType = waterType;
    }

    public static void writeFishDetailsToTextFile(Map<Fish, Owner> fishes)
    {
        // write Fish details to file
        try (FileWriter petDetailsFile = new FileWriter("petDetailsFile.txt", true))
        {
            fishes.forEach((key, value) ->
            {
                try
                {
                    petDetailsFile.write(key.getPetId() + "," + key.getOwnerId() + "," + key.getType()
                            + "," + key.getWaterType() + "," + key.getName() + ","
                            + key.getBreed() + "," + key.getColour() + "," + key.getGender()
                            + "," + key.getDateRegistered() + ","
                            + key.getAge() + "," + value.getOwnerId() + "," + value.getName() + "," + value.getEmail() + ","
                            + value.getTelephone() + "," + value.getHomeAddress() + ",");
                } catch (IOException e)
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
                }

            });
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            return;
        }

    }

    public static void writeFishDetailsToBinaryFile(Map<Fish, Owner> fishes)
    {
        try (DataOutputStream petDetailsBinFile = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(("petDetailsFile.bin")))))
        {

            fishes.forEach((key, value) ->
            {
                try
                {
                    petDetailsBinFile.writeUTF(key.getPetId() + "," + key.getOwnerId() + "," + key.getType()
                            + "," + key.getWaterType() + "," + key.getName() + ","
                            + key.getBreed() + "," + key.getColour() + "," + key.getGender()
                            + "," + key.getDateRegistered() + ","
                            + key.getAge() + " | " + value.getOwnerId() + "," + value.getName() + "," + value.getEmail() + ","
                            + value.getTelephone() + "," + value.getHomeAddress() + "\n");
                } catch (IOException e)
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
                }

            });
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            return;
        }

    }

    public static void writeFishOS(Map<Fish, Owner> fishes)
    {
        try (ObjectOutputStream fishDetailsFileOS = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("fishDetailsFileOS.ser"))))
        {
            fishDetailsFileOS.writeObject(fishes);

        } catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    public static void readFishOS() throws IOException
    {

        try (ObjectInputStream fishDetailsFileOS = new ObjectInputStream(new BufferedInputStream(new FileInputStream("fishDetailsFileOS.ser"))))
        {
            Map<Fish, Owner> fishes;
            fishes = (LinkedHashMap) fishDetailsFileOS.readObject();
            Pets.getFishes().putAll(fishes);
            //Pets.getPets().putAll(Pets.getFishes());

        } catch (InvalidClassException ie)
        {
            System.out.println("Invalid Class");
        } catch (FileNotFoundException | ClassNotFoundException ex)
        {
            Logger.getLogger(Fish.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.waterType);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Fish other = (Fish) obj;
        if (!Objects.equals(this.waterType, other.waterType))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "Fish{" + super.toString() + "waterType=" + waterType + '}';
    }

}
